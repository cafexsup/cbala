//
//  AppDelegate.h
//  CBA
//
//  Created by Richard Morgan on 19/12/2016.
//  Copyright © 2016 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

